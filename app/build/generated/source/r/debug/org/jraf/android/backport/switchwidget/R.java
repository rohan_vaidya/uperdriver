/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package org.jraf.android.backport.switchwidget;

public final class R {
	public static final class attr {
		public static final int asb_disableDependentsState = 0x7f010123;
		public static final int asb_summaryOff = 0x7f010120;
		public static final int asb_summaryOn = 0x7f01011f;
		public static final int asb_switchMinWidth = 0x7f010110;
		public static final int asb_switchPadding = 0x7f010111;
		public static final int asb_switchPreferenceStyle = 0x7f010113;
		public static final int asb_switchStyle = 0x7f010112;
		public static final int asb_switchTextAppearance = 0x7f01010f;
		public static final int asb_switchTextOff = 0x7f010122;
		public static final int asb_switchTextOn = 0x7f010121;
		public static final int asb_textOff = 0x7f01010d;
		public static final int asb_textOn = 0x7f01010c;
		public static final int asb_thumb = 0x7f01010a;
		public static final int asb_thumbTextPadding = 0x7f01010e;
		public static final int asb_track = 0x7f01010b;
	}
	public static final class color {
		public static final int background_holo_light = 0x7f0c0009;
		public static final int bright_foreground_disabled_holo_dark = 0x7f0c0011;
		public static final int bright_foreground_holo_dark = 0x7f0c0014;
		public static final int dim_foreground_disabled_holo_dark = 0x7f0c004c;
		public static final int dim_foreground_holo_dark = 0x7f0c004f;
		public static final int primary_text_holo_dark = 0x7f0c00af;
		public static final int secondary_text_holo_dark = 0x7f0c00b0;
	}
	public static final class dimen {
		public static final int preference_icon_minWidth = 0x7f070067;
		public static final int preference_item_padding_inner = 0x7f0700c4;
		public static final int preference_item_padding_side = 0x7f0700c5;
		public static final int preference_widget_width = 0x7f07005a;
	}
	public static final class drawable {
		public static final int switch_bg_disabled_holo_dark = 0x7f020124;
		public static final int switch_bg_disabled_holo_light = 0x7f020125;
		public static final int switch_bg_focused_holo_dark = 0x7f020126;
		public static final int switch_bg_focused_holo_light = 0x7f020127;
		public static final int switch_bg_holo_dark = 0x7f020128;
		public static final int switch_bg_holo_light = 0x7f020129;
		public static final int switch_inner_holo_dark = 0x7f02012a;
		public static final int switch_inner_holo_light = 0x7f02012b;
		public static final int switch_thumb_activated_holo_dark = 0x7f02012c;
		public static final int switch_thumb_activated_holo_light = 0x7f02012d;
		public static final int switch_thumb_disabled_holo_dark = 0x7f02012e;
		public static final int switch_thumb_disabled_holo_light = 0x7f02012f;
		public static final int switch_thumb_holo_dark = 0x7f020130;
		public static final int switch_thumb_holo_light = 0x7f020131;
		public static final int switch_thumb_holo_light_v2 = 0x7f020132;
		public static final int switch_thumb_pressed_holo_dark = 0x7f020133;
		public static final int switch_thumb_pressed_holo_light = 0x7f020134;
		public static final int switch_track_holo_dark = 0x7f020135;
		public static final int switch_track_holo_light = 0x7f020136;
	}
	public static final class id {
		public static final int switchWidget = 0x7f0d0173;
	}
	public static final class layout {
		public static final int preference = 0x7f03005a;
		public static final int preference_widget_switch = 0x7f03005b;
	}
	public static final class string {
		public static final int switch_off = 0x7f0600de;
		public static final int switch_on = 0x7f0600df;
	}
	public static final class style {
		public static final int Preference_SwitchPreference = 0x7f0900c9;
		public static final int TextAppearance_Holo_Light_Widget_Switch = 0x7f0900f7;
		public static final int TextAppearance_Holo_Widget_Switch = 0x7f0900f8;
		public static final int Widget_Holo_CompoundButton_Switch = 0x7f090161;
		public static final int Widget_Holo_Light_CompoundButton_Switch = 0x7f090162;
	}
	public static final class styleable {
		public static final int[] Android = { 0x01010095, 0x01010096, 0x01010097, 0x01010098, 0x01010099, 0x0101009a, 0x0101009b };
		public static final int Android_android_textColor = 3;
		public static final int Android_android_textColorHighlight = 4;
		public static final int Android_android_textColorHint = 5;
		public static final int Android_android_textColorLink = 6;
		public static final int Android_android_textSize = 0;
		public static final int Android_android_textStyle = 2;
		public static final int Android_android_typeface = 1;
		public static final int[] Switch = { 0x7f01010a, 0x7f01010b, 0x7f01010c, 0x7f01010d, 0x7f01010e, 0x7f01010f, 0x7f010110, 0x7f010111 };
		public static final int[] SwitchBackportTheme = { 0x7f010112, 0x7f010113 };
		public static final int SwitchBackportTheme_asb_switchPreferenceStyle = 1;
		public static final int SwitchBackportTheme_asb_switchStyle = 0;
		public static final int[] SwitchPreference = { 0x7f01011f, 0x7f010120, 0x7f010121, 0x7f010122, 0x7f010123 };
		public static final int SwitchPreference_asb_disableDependentsState = 4;
		public static final int SwitchPreference_asb_summaryOff = 1;
		public static final int SwitchPreference_asb_summaryOn = 0;
		public static final int SwitchPreference_asb_switchTextOff = 3;
		public static final int SwitchPreference_asb_switchTextOn = 2;
		public static final int Switch_asb_switchMinWidth = 6;
		public static final int Switch_asb_switchPadding = 7;
		public static final int Switch_asb_switchTextAppearance = 5;
		public static final int Switch_asb_textOff = 3;
		public static final int Switch_asb_textOn = 2;
		public static final int Switch_asb_thumb = 0;
		public static final int Switch_asb_thumbTextPadding = 4;
		public static final int Switch_asb_track = 1;
	}
}
