/**
 * 
 */
package com.uper.provider.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.uper.provider.R;
import com.uper.provider.model.SubSubType;
import com.uper.provider.widget.MyFontTextView;

/**
 * @author Elluminati elluminati.in
 * 
 */
public class SubSubCategoryListAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private ViewHolder holder;
	private ArrayList<SubSubType> listDriver;
	private Context context;
	private ImageOptions imageOptions;
	private AQuery aQuery;

	public SubSubCategoryListAdapter(Context context,
			ArrayList<SubSubType> listDriver) {
		this.context = context;
		this.listDriver = listDriver;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageOptions = new ImageOptions();
		imageOptions.fileCache = true;
		imageOptions.memCache = true;
		imageOptions.fallback = R.drawable.user;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return listDriver.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) {
		return listDriver.get(position);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.row_sub_sub_category,
					parent, false);
			aQuery = new AQuery(context);
			holder = new ViewHolder();
			holder.tvSubSubCateName = (MyFontTextView) convertView
					.findViewById(R.id.tvSubSubCateName);
			holder.ivImg = (ImageView) convertView.findViewById(R.id.ivImg);
			holder.tvDescription = (MyFontTextView) convertView
					.findViewById(R.id.tvDescription);
			holder.tvPrice = (MyFontTextView) convertView
					.findViewById(R.id.tvPrice);
			holder.tvQuantity = (MyFontTextView) convertView
					.findViewById(R.id.tvQuantity);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		SubSubType driver = listDriver.get(position);

		holder.ivImg.setTag(position);
		aQuery.id(holder.ivImg).progress(R.id.pBar)
				.image(driver.getIcon(), imageOptions);

		holder.tvSubSubCateName.setText(driver.getName());
		holder.tvDescription.setText(driver.getDescription());
		holder.tvPrice.setText(driver.getPrice() + "");
		holder.tvQuantity.setText(driver.getQuantity() + "");

		return convertView;
	}

	private class ViewHolder {
		MyFontTextView tvSubSubCateName, tvDescription, tvPrice, tvQuantity;
		ImageView ivImg;
	}

}
