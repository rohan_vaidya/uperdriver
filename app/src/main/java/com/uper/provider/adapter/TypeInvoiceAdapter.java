package com.uper.provider.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.uper.provider.R;
import com.uper.provider.model.TypeInvoice;

public class TypeInvoiceAdapter extends BaseAdapter {
	ArrayList<TypeInvoice> listTypeInvoice;
	Context context;
	private ViewHolder holder;
	private LayoutInflater inflater;

	public TypeInvoiceAdapter(Context context,
			ArrayList<TypeInvoice> listTypeInvoice) {
		this.context = context;
		this.listTypeInvoice = listTypeInvoice;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		return listTypeInvoice.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_type_invoice, parent,
					false);
			holder = new ViewHolder();
			holder.tvTypeInvoiceName = (TextView) convertView
					.findViewById(R.id.tvTypeInvoiceName);
			holder.tvTypeInvoicePrice = (TextView) convertView
					.findViewById(R.id.tvTypeInvoicePrice);
			holder.tvTypeInvoiceQuantity = (TextView) convertView
					.findViewById(R.id.tvTypeInvoiceQuantity);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		TypeInvoice typeInvoice = listTypeInvoice.get(position);
		holder.tvTypeInvoiceName.setText(typeInvoice.getName());
		holder.tvTypeInvoiceQuantity.setText(typeInvoice.getQuantity());
		holder.tvTypeInvoicePrice.setText(context.getResources().getString(
				R.string.currency)
				+ "" + typeInvoice.getBasePrice());

		return convertView;
	}

	private class ViewHolder {
		TextView tvTypeInvoiceName, tvTypeInvoicePrice, tvTypeInvoiceQuantity;
	}

}
