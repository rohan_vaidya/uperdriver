package com.uper.provider;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;

import com.uper.provider.base.ActionBarBaseActivitiy;
import com.uper.provider.utills.AndyConstants;

public class TermsConditionsActivity extends ActionBarBaseActivitiy {

	private WebView wvTermsConditions;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_terms_conditions);
		btnActionMenu.setVisibility(View.INVISIBLE);
		btnNotification.setVisibility(View.INVISIBLE);
		tvTitle.setText(getResources().getString(R.string.text_t_c));
		wvTermsConditions = (WebView) findViewById(R.id.wvTermsConditions);
		wvTermsConditions
				.loadUrl(AndyConstants.ServiceType.TERMS_AND_CONDITIONS);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		// TODO Auto-generated method stub
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

}
