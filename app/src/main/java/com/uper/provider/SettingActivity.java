package com.uper.provider;

import java.util.HashMap;

import org.jraf.android.backport.switchwidget.Switch;

import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.uper.provider.base.ActionBarBaseActivitiy;
import com.uper.provider.parse.HttpRequester;
import com.uper.provider.utills.AndyConstants;
import com.uper.provider.utills.AndyUtils;
import com.uper.provider.utills.AppLog;
import com.uper.provider.utills.PreferenceHelper;

/**
 * @author Elluminati elluminati.in
 * 
 */
public class SettingActivity extends ActionBarBaseActivitiy implements
		OnCheckedChangeListener {
	private Switch switchSetting;
	private SwitchCompat switchSound;
	private PreferenceHelper preferenceHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		preferenceHelper = new PreferenceHelper(this);
		switchSetting = (Switch) findViewById(R.id.switchAvaibility);
		switchSetting.setVisibility(View.GONE);
		switchSound = (SwitchCompat) findViewById(R.id.switchSound);
		setActionBarTitle(getString(R.string.text_setting));
		setActionBarIcon(R.drawable.promotion);
		switchSound.setChecked(preferenceHelper.getSoundAvailability());
		switchSound.setOnCheckedChangeListener(this);

	}

	private void changeState() {
		if (!AndyUtils.isNetworkAvailable(this)) {
			AndyUtils.showToast(
					getResources().getString(R.string.toast_no_internet), this);
			return;
		}

		AndyUtils.showCustomProgressDialog(this, "",
				getResources().getString(R.string.progress_changing_avaibilty),
				false);

		HashMap<String, String> map = new HashMap<String, String>();
		map.put(AndyConstants.URL, AndyConstants.ServiceType.TOGGLE_STATE);
		map.put(AndyConstants.Params.ID, preferenceHelper.getUserId());
		map.put(AndyConstants.Params.TOKEN, preferenceHelper.getSessionToken());

		new HttpRequester(this, map, AndyConstants.ServiceCode.TOGGLE_STATE,
				this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			break;

		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		AppLog.Log("TAG", "On checked change listener");
		switch (buttonView.getId()) {
		case R.id.switchAvaibility:
			changeState();
			break;

		case R.id.switchSound:
			AppLog.Log("Setting Activity Sound switch",
					"" + switchSound.isChecked());
			preferenceHelper.putSoundAvailability(switchSound.isChecked());

			break;
		default:
			break;
		}
	}

	// @Override
	// public void onTaskCompleted(String response, int serviceCode) {
	// AndyUtils.removeCustomProgressDialog();
	// switch (serviceCode) {
	// case AndyConstants.ServiceCode.CHECK_STATE:
	// case AndyConstants.ServiceCode.TOGGLE_STATE:
	//
	// preferenceHelper.putIsActive(false);
	// preferenceHelper.putDriverOffline(false);
	// if (!parseContent.isSuccess(response)) {
	// return;
	// }
	// AppLog.Log("TAG", "toggle state:" + response);
	// if (parseContent.parseAvaibilty(response)) {
	// switchSetting.setOnCheckedChangeListener(null);
	// switchSetting.setChecked(true);
	//
	// } else {
	// switchSetting.setOnCheckedChangeListener(null);
	// switchSetting.setChecked(false);
	// }
	// switchSetting.setOnCheckedChangeListener(this);
	// break;
	// default:
	// break;
	// }
	// }

	@Override
	protected void onDestroy() {
		super.onDestroy();
		AndyUtils.removeCustomProgressDialog();
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btnActionNotification:
			onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;
		}

	}
}
