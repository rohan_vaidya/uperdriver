package com.uper.provider.model;

import java.io.Serializable;
import java.util.ArrayList;

public class SubType implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String description;
	private int type;
	private ArrayList<SubSubType> sub_subtype = new ArrayList<SubSubType>();
	private String icon;
	
	public boolean isSelected;
	
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	private int id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public ArrayList<SubSubType> getSub_subtype() {
		return sub_subtype;
	}
	public void setSub_subtype(ArrayList<SubSubType> sub_subtype) {
		this.sub_subtype = sub_subtype;
	}
	
	
	
}
