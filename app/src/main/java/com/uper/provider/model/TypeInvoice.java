package com.uper.provider.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class TypeInvoice implements Serializable {

	private String name, quantity;
	private double basePrice;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}
}
