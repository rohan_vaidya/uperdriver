package com.uper.provider.fragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.Plus.PlusOptions;
import com.google.android.gms.plus.model.people.Person;
import com.soundcloud.android.crop.Crop;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.Permission.Type;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.entities.Profile.Properties;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.uper.provider.MenuDescActivity;
import com.uper.provider.R;
import com.uper.provider.RegisterActivity;
import com.uper.provider.TermsConditionsActivity;
import com.uper.provider.adapter.VehicalTypeListAdapter;
import com.uper.provider.base.BaseRegisterFragment;
import com.uper.provider.model.VehicalType;
import com.uper.provider.parse.AsyncTaskCompleteListener;
import com.uper.provider.parse.HttpRequester;
import com.uper.provider.parse.MultiPartRequester;
import com.uper.provider.parse.ParseContent;
import com.uper.provider.utills.AndyConstants;
import com.uper.provider.utills.AndyUtils;
import com.uper.provider.utills.AppLog;
import com.uper.provider.utills.PreferenceHelper;
import com.uper.provider.widget.MyFontButton;
import com.uper.provider.widget.MyFontEdittextView;
import com.uper.provider.widget.MyFontTextView;

/**
 * @author Elluminati elluminati.in
 * 
 */
public class RegisterFragment extends BaseRegisterFragment implements
		OnClickListener, ConnectionCallbacks, OnConnectionFailedListener,
		AsyncTaskCompleteListener {
	private MyFontEdittextView etRegisterFname, etRegisterLName,
			etRegisterPassword, etRegisterEmail, etRegisterNumber,
			etRegisterAddress, etRegisterBio, etRegisterZipcode, etDescription,
			etLicensePlatNo, etRegisterState, etRegisterCountry,
			etRegisterCarModel;
	private MyFontButton btnRegisterEmailInfo;
	private MyFontTextView tvCountryCode, tvPopupMsg;
	private ImageButton btnFb, btnGplus;
	// private GridView gvTypes;
	private SlidingDrawer drawer;
	private ImageView ivProfile;
	private boolean mSignInClicked, mIntentInProgress;
	private ConnectionResult mConnectionResult;
	private GoogleApiClient mGoogleApiClient;
	private AQuery aQuery;
	private SimpleFacebook mSimpleFacebook;
	private ParseContent parseContent;
	private ArrayList<String> countryList;
	private String country;
	private Uri uri = null;
	private String loginType = AndyConstants.MANUAL, socialId,
			profileImageData = null, socialProPicUrl;
	private String filePath = null, profileImageFilePath;
	private int rotationAngle;
	private Bitmap photoBitmap;
	private ImageOptions profileImageOptions;
	private SimpleFacebookConfiguration facebookConfiguration;
	// private ArrayList<VehicalType> listType;
	// private VehicalTypeListAdapter adapter;
	private PopupWindow registerInfoPopup;

	private final String TAG = "RegisterFragment";
	private static final int RC_SIGN_IN = 0;

	private int selectedPostion = -1, is_skip = 0;
	private GridView listViewType;
	private ArrayList<VehicalType> listType;

	private VehicalTypeListAdapter typeAdapter;
	private boolean isGettingVehicalType = true;
	private CheckBox cbAccept;
	private MyFontTextView tvTermsCondition;
	// private int selectedTypePostion = -1;ivRegisterProfile
	// private RequestQueue requestQueue;

	Permission[] facebookPermission = new Permission[] { Permission.EMAIL };

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View registerFragmentView = inflater.inflate(
				R.layout.fragment_register, container, false);

		ivProfile = (ImageView) registerFragmentView
				.findViewById(R.id.ivRegisterProfile);
		etRegisterAddress = (MyFontEdittextView) registerFragmentView
				.findViewById(R.id.etRegisterAddress);
		etRegisterBio = (MyFontEdittextView) registerFragmentView
				.findViewById(R.id.etRegisterBio);
		etRegisterEmail = (MyFontEdittextView) registerFragmentView
				.findViewById(R.id.etRegisterEmail);
		etRegisterFname = (MyFontEdittextView) registerFragmentView
				.findViewById(R.id.etRegisterFName);
		etRegisterLName = (MyFontEdittextView) registerFragmentView
				.findViewById(R.id.etRegisterLName);
		etRegisterNumber = (MyFontEdittextView) registerFragmentView
				.findViewById(R.id.etRegisterNumber);
		etRegisterPassword = (MyFontEdittextView) registerFragmentView
				.findViewById(R.id.etRegisterPassword);
		etRegisterZipcode = (MyFontEdittextView) registerFragmentView
				.findViewById(R.id.etRegisterZipCode);
		etRegisterState = (MyFontEdittextView) registerFragmentView
				.findViewById(R.id.etRegisterState);
		etRegisterCountry = (MyFontEdittextView) registerFragmentView
				.findViewById(R.id.etRegisterCountry);
		etDescription = (MyFontEdittextView) registerFragmentView
				.findViewById(R.id.etDescription);
		etRegisterCarModel = (MyFontEdittextView) registerFragmentView
				.findViewById(R.id.etRegisterCarModel);
		etLicensePlatNo = (MyFontEdittextView) registerFragmentView
				.findViewById(R.id.etLicensePlatNo);
		cbAccept = (CheckBox) registerFragmentView.findViewById(R.id.cbAccept);
		tvTermsCondition = (MyFontTextView) registerFragmentView
				.findViewById(R.id.tvTermsCondition);
		tvCountryCode = (MyFontTextView) registerFragmentView
				.findViewById(R.id.tvRegisterCountryCode);
		btnRegisterEmailInfo = (MyFontButton) registerFragmentView
				.findViewById(R.id.btnRegisterEmailInfo);
		// btnRegisterModelInfo = (MyFontButton) registerFragmentView
		// .findViewById(R.id.btnRegisterModelInfo);
		// btnRegisterTaxiNoInfo = (MyFontButton) registerFragmentView
		// .findViewById(R.id.btnRegisterTaxiNoInfo);
		btnFb = (ImageButton) registerFragmentView
				.findViewById(R.id.btnRegisterFb);
		btnGplus = (ImageButton) registerFragmentView
				.findViewById(R.id.btnRegisterGplus);
		drawer = (SlidingDrawer) registerFragmentView.findViewById(R.id.drawer);

		// gvTypes = (GridView) registerFragmentView.findViewById(R.id.gvTypes);
		tvTermsCondition.setOnClickListener(this);
		tvCountryCode.setOnClickListener(this);
		ivProfile.setOnClickListener(this);
		btnRegisterEmailInfo.setOnClickListener(this);
		// btnRegisterModelInfo.setOnClickListener(this);
		// btnRegisterTaxiNoInfo.setOnClickListener(this);
		registerFragmentView.findViewById(R.id.btnRegisterFb)
				.setOnClickListener(this);
		registerFragmentView.findViewById(R.id.btnRegisterGplus)
				.setOnClickListener(this);
		registerFragmentView.findViewById(R.id.tvRegisterSubmit)
				.setOnClickListener(this);

		selectedPostion = 0;
		listViewType = (GridView) registerFragmentView
				.findViewById(R.id.gvTypes);
		listType = new ArrayList<VehicalType>();
		typeAdapter = new VehicalTypeListAdapter(registerActivity, listType);
		listViewType.setAdapter(typeAdapter);
		getVehicalTypes();
		// drawer.lock();
		listViewType.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				if (listType.get(position).isSelected) {
					listType.get(position).isSelected = false;
				} else {
					listType.get(position).isSelected = true;
				}

				selectedPostion = position;
				typeAdapter.notifyDataSetChanged();

			}
		});

		return registerFragmentView;
	}

	private String getSelectedType() {
		ArrayList<Integer> listSelectedType = new ArrayList<Integer>();
		for (int i = 0; i < listType.size(); i++) {
			if (listType.get(i).isSelected)
				listSelectedType.add(listType.get(i).getId());

		}
		return TextUtils.join(",", listSelectedType);
	}

	private void getVehicalTypes() {
		AndyUtils.showCustomProgressDialog(registerActivity, "", getResources()
				.getString(R.string.progress_get_type), false);
		isGettingVehicalType = true;
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(AndyConstants.URL, AndyConstants.ServiceType.GET_VEHICAL_TYPES);
		new HttpRequester(registerActivity, map,
				AndyConstants.ServiceCode.GET_VEHICAL_TYPES, true, this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		registerActivity.actionBar.show();
		registerActivity.setActionBarTitle(getResources().getString(
				R.string.text_register));
		registerActivity.setActionBarIcon(R.drawable.ic_launcher);
		registerActivity.btnActionInfo.setVisibility(View.VISIBLE);
		registerActivity.btnActionInfo.setOnClickListener(this);
		parseContent = new ParseContent(registerActivity);

		// popup
		LayoutInflater inflate = LayoutInflater.from(registerActivity);
		RelativeLayout layout = (RelativeLayout) inflate.inflate(
				R.layout.popup_notification_window, null);
		tvPopupMsg = (MyFontTextView) layout.findViewById(R.id.tvPopupMsg);
		registerInfoPopup = new PopupWindow(layout, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);

		layout.setOnClickListener(this);
		registerInfoPopup.setBackgroundDrawable(new BitmapDrawable());
		registerInfoPopup.setOutsideTouchable(true);

		aQuery = new AQuery(registerActivity);
		profileImageOptions = new ImageOptions();
		profileImageOptions.fileCache = true;
		profileImageOptions.memCache = true;
		profileImageOptions.targetWidth = 200;
		profileImageOptions.fallback = R.drawable.user;

		// listType = new ArrayList<VehicalType>();
		// adapter = new VehicalTypeListAdapter(registerActivity, listType,
		// this);
		// gvTypes.setAdapter(adapter);
		// gvTypes.setOnItemClickListener(new OnItemClickListener() {
		//
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view,
		// int position, long id) {
		//
		// if (listType.get(position).isSelected) {
		// listType.get(position).isSelected = false;
		// } else {
		// listType.get(position).isSelected = true;
		// }
		//
		// // onItemClick(position);
		// selectedTypePostion = position;
		// adapter.notifyDataSetChanged();
		// }
		// });

		countryList = parseContent.parseCountryCodes();
		for (int i = 0; i < countryList.size(); i++) {
			if (countryList.get(i).contains(country)) {
				tvCountryCode.setText((countryList.get(i).substring(0,
						countryList.get(i).indexOf(" "))));
			}
		}
		if (TextUtils.isEmpty(tvCountryCode.getText())) {
			tvCountryCode.setText((countryList.get(0).substring(0, countryList
					.get(0).indexOf(" "))));
		}
		// getVehicalTypes();
		// facebook api initialization
		facebookConfiguration = new SimpleFacebookConfiguration.Builder()
				.setAppId(getResources().getString(R.string.app_id))
				.setNamespace(getResources().getString(R.string.app_name))
				.setPermissions(facebookPermission).build();
		SimpleFacebook.setConfiguration(facebookConfiguration);

		// Google plus api initialization
		Scope scope = new Scope(AndyConstants.GOOGLE_API_SCOPE_URL);
		mGoogleApiClient = new GoogleApiClient.Builder(registerActivity)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(Plus.API, PlusOptions.builder().build())
				.addScope(scope).build();

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		country = Locale.getDefault().getDisplayCountry();
		// requestQueue = Volley.newRequestQueue(registerActivity);
		facebookConfiguration = new SimpleFacebookConfiguration.Builder()
				.setAppId(getResources().getString(R.string.app_id))
				.setNamespace(getResources().getString(R.string.app_name))
				.setPermissions(facebookPermission).build();
		SimpleFacebook.setConfiguration(facebookConfiguration);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btnRegisterFb:
			if (!mSimpleFacebook.isLogin()) {
				registerActivity.setFbTag(AndyConstants.REGISTER_FRAGMENT_TAG);
				mSimpleFacebook.login(new OnLoginListener() {

					@Override
					public void onFail(String arg0) {
						Toast.makeText(
								registerActivity,
								getResources().getString(
										R.string.toast_facebook_login_failed),
								Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onException(Throwable arg0) {
					}

					@Override
					public void onThinking() {
					}

					@Override
					public void onNotAcceptingPermissions(Type arg0) {
						AppLog.Log("UBER",
								String.format(
										"You didn't accept %s permissions",
										arg0.name()));
					}

					@Override
					public void onLogin() {
						// Toast.makeText(registerActivity, "Facebook Success",
						// Toast.LENGTH_SHORT).show();
					}
				});
			} else {
				getFbProfile();
			}
			break;

		case R.id.ivRegisterProfile:
			showPictureDialog();
			break;

		case R.id.btnRegisterGplus:
			mSignInClicked = true;
			if (!mGoogleApiClient.isConnecting()) {
				AndyUtils.showCustomProgressDialog(registerActivity, "",
						getString(R.string.progress_getting_info), false);
				mGoogleApiClient.connect();
			}
			break;

		case R.id.tvRegisterSubmit:
			onRegisterButtonClick();
			break;

		case R.id.tvTermsCondition:
			Intent termsIntent = new Intent(registerActivity,
					TermsConditionsActivity.class);
			startActivity(termsIntent);
			break;

		case R.id.tvRegisterCountryCode:
			AlertDialog.Builder countryBuilder = new Builder(registerActivity);
			countryBuilder.setTitle(getResources().getString(
					R.string.dialog_title_country_codes));

			final String[] countryListArray = new String[countryList.size()];
			countryList.toArray(countryListArray);
			countryBuilder.setItems(countryListArray,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							tvCountryCode.setText(countryListArray[which]
									.substring(0, countryListArray[which]
											.indexOf(" ")));
						}
					}).show();
			break;
		case R.id.btnRegisterEmailInfo:
			openPopup(btnRegisterEmailInfo,
					getString(R.string.text_regemail_popup));
			break;

		// case R.id.btnRegisterModelInfo:
		// openPopup(btnRegisterModelInfo,
		// getString(R.string.text_regmodelno_popup));
		// break;
		//
		// case R.id.btnRegisterTaxiNoInfo:
		// openPopup(btnRegisterTaxiNoInfo,
		// getString(R.string.text_regtaxino_popup));
		// break;

		case R.id.btnActionInfo:
			openPopup(registerActivity.btnActionInfo,
					getString(R.string.text_regaction_popup));
			break;

		default:
			break;
		}
	}

	public void openPopup(View view, String msg) {
		if (registerInfoPopup.isShowing())
			registerInfoPopup.dismiss();
		else {
			registerInfoPopup.showAsDropDown(view);
			tvPopupMsg.setText(msg);
		}

	}

	private void onRegisterButtonClick() {
		if (etRegisterFname.getText().length() == 0) {
			AndyUtils.showToast(
					getResources().getString(R.string.error_empty_fname),
					registerActivity);
			return;
		} else if (etRegisterLName.getText().length() == 0) {
			AndyUtils.showToast(
					getResources().getString(R.string.error_empty_lname),
					registerActivity);
			return;
		} else if (etRegisterEmail.getText().length() == 0) {
			AndyUtils.showToast(
					getResources().getString(R.string.error_empty_email),
					registerActivity);
			return;
		}
		// else if(etDescription.getText().length() == 0)
		// {
		// AndyUtils.showToast(
		// getResources().getString(R.string.error_empty_description),
		// registerActivity);
		// return;
		// }
		else if (getSelectedType().length() == 0) {
			AndyUtils.showToast(
					getResources().getString(R.string.error_empty_type),
					registerActivity);
			return;
		}

		else if (etLicensePlatNo.getText().length() == 0) {
			AndyUtils.showToast(
					getResources().getString(R.string.error_empty_license),
					registerActivity);
			return;
		} else if (!AndyUtils.eMailValidation(etRegisterEmail.getText()
				.toString())) {
			AndyUtils.showToast(
					getResources().getString(R.string.error_valid_email),
					registerActivity);
			return;
			// } else if (etRegisterModel.getText().length() == 0) {
			// AndyUtils.showToast(
			// getResources().getString(R.string.error_empty_model),
			// registerActivity);
			// return;
			// } else if (etRegisterTaxiNo.getText().length() == 0) {
			// AndyUtils.showToast(
			// getResources().getString(R.string.error_empty_taxi_no),
			// registerActivity);
			// return;

		} else if (etRegisterPassword.getVisibility() == View.VISIBLE) {
			if (etRegisterPassword.getText().length() == 0) {
				AndyUtils.showToast(
						getResources().getString(
								R.string.error_empty_reg_password),
						registerActivity);
				return;
			} else if (etRegisterPassword.getText().length() < 6) {
				AndyUtils
						.showToast(
								getResources().getString(
										R.string.error_valid_password),
								registerActivity);
				return;
			}
		}

		if (etRegisterPassword.getVisibility() == View.GONE) {
			if (!TextUtils.isEmpty(socialProPicUrl)) {
				profileImageData = null;
				profileImageData = aQuery.getCachedFile(socialProPicUrl)
						.getPath();
			}
		}

		if (!cbAccept.isChecked()) {
			AndyUtils.showToast(
					getString(R.string.text_please_accept_terms_conditions),
					registerActivity);
			return;
		}

		if (etRegisterNumber.getText().length() == 0) {
			AndyUtils.showToast(
					getResources().getString(R.string.error_empty_number),
					registerActivity);
			return;
			// } else if (profileImageData == null ||
			// profileImageData.equals("")) {
			// AndyUtils.showToast(
			// getResources().getString(R.string.error_empty_image),
			// registerActivity);
			// return;
			// } else if (selectedTypePostion == -1) {
			// AndyUtils.showToast(
			// getResources().getString(R.string.error_empty_type),
			// registerActivity);
			// // drawer.open();
			// return;
		} else {
			register(loginType, socialId);
		}
	}

	private void showPictureDialog() {
		AlertDialog.Builder pictureDialog = new AlertDialog.Builder(
				registerActivity);
		pictureDialog.setTitle(getResources().getString(
				R.string.dialog_chhose_photo));
		String[] pictureDialogItems = {
				getResources().getString(R.string.dialog_from_gallery),
				getResources().getString(R.string.dialog_from_camera) };

		pictureDialog.setItems(pictureDialogItems,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {

						case 0:
							choosePhotoFromGallary();
							break;

						case 1:
							takePhotoFromCamera();
							break;

						}
					}
				});
		pictureDialog.show();
	}

	private void resolveSignInError() {
		if (mConnectionResult.hasResolution()) {
			try {
				mIntentInProgress = true;
				registerActivity.startIntentSenderForResult(mConnectionResult
						.getResolution().getIntentSender(), RC_SIGN_IN, null,
						0, 0, 0, AndyConstants.REGISTER_FRAGMENT_TAG);
			} catch (SendIntentException e) {
				/*
				 * The intent was canceled before it was sent. Return to the
				 * default state and attempt to connect to get an updated
				 * ConnectionResult.
				 */
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (!mIntentInProgress) {
			mConnectionResult = result;
			if (mSignInClicked) {
				resolveSignInError();
			}
		}

	}

	private void getFbProfile() {
		AndyUtils.showCustomProgressDialog(registerActivity, "",
				getString(R.string.text_getting_info_facebook), true);

		Profile.Properties properties = new Profile.Properties.Builder()
				.add(Properties.ID).add(Properties.FIRST_NAME)
				.add(Properties.GENDER).add(Properties.EMAIL)
				.add(Properties.LAST_NAME).add(Properties.BIRTHDAY)
				.add(Properties.EDUCATION).add(Properties.PICTURE).build();
		mSimpleFacebook.getProfile(properties, new OnProfileListener() {
			@Override
			public void onComplete(Profile profile) {
				AndyUtils.removeCustomProgressDialog();
				AppLog.Log("Uber", "My profile id = " + profile.getId());
				btnFb.setEnabled(false);
				btnGplus.setEnabled(false);
				etRegisterEmail.setText(profile.getEmail());
				etRegisterFname.setText(profile.getFirstName());
				etRegisterLName.setText(profile.getLastName());
				socialId = profile.getId();
				loginType = AndyConstants.SOCIAL_FACEBOOK;
				// etRegisterPassword.setEnabled(false);
				etRegisterPassword.setVisibility(View.GONE);

				if (!TextUtils.isEmpty(profile.getPicture())
						|| !profile.getPicture().equalsIgnoreCase("null")) {
					socialProPicUrl = profile.getPicture();
					aQuery.id(ivProfile).image(profile.getPicture(),
							profileImageOptions);
				} else {
					socialProPicUrl = null;
				}

			}
		});
	}

	public void onItemClick(int pos) {
		// selectedTypePostion = pos;

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RC_SIGN_IN) {

			if (resultCode != RegisterActivity.RESULT_OK) {
				mSignInClicked = false;
			}

			mIntentInProgress = false;

			if (!mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}
		} else if (requestCode == AndyConstants.CHOOSE_PHOTO) {
			if (data != null) {
				Uri uri = data.getData();

				AppLog.Log(TAG, "Choose photo on activity result");

				profileImageFilePath = getRealPathFromURI(uri);
				filePath = profileImageFilePath;
				try {
					int mobile_width = 480;
					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inJustDecodeBounds = true;
					BitmapFactory.decodeFile(filePath, options);
					int outWidth = options.outWidth;
					int ratio = (int) ((((float) outWidth) / mobile_width) + 0.5f);

					if (ratio == 0) {
						ratio = 1;
					}
					ExifInterface exif = new ExifInterface(filePath);

					String orientString = exif
							.getAttribute(ExifInterface.TAG_ORIENTATION);
					int orientation = orientString != null ? Integer
							.parseInt(orientString)
							: ExifInterface.ORIENTATION_NORMAL;

					if (orientation == ExifInterface.ORIENTATION_ROTATE_90)
						rotationAngle = 90;
					if (orientation == ExifInterface.ORIENTATION_ROTATE_180)
						rotationAngle = 180;
					if (orientation == ExifInterface.ORIENTATION_ROTATE_270)
						rotationAngle = 270;

					System.out.println("Rotation : " + rotationAngle);

					options.inJustDecodeBounds = false;
					options.inSampleSize = ratio;

					photoBitmap = BitmapFactory.decodeFile(filePath, options);
					if (photoBitmap != null) {
						Matrix matrix = new Matrix();
						matrix.setRotate(rotationAngle,
								(float) photoBitmap.getWidth() / 2,
								(float) photoBitmap.getHeight() / 2);
						photoBitmap = Bitmap.createBitmap(photoBitmap, 0, 0,
								photoBitmap.getWidth(),
								photoBitmap.getHeight(), matrix, true);

						AppLog.Log(TAG, "Take photo on activity result");
						String path = Images.Media.insertImage(
								registerActivity.getContentResolver(),
								photoBitmap, Calendar.getInstance()
										.getTimeInMillis() + ".jpg", null);

						if (!TextUtils.isEmpty(path)) {
							beginCrop(Uri.parse(path));
						} else {
							AndyUtils
									.showToast(
											getResources()
													.getString(
															R.string.text_unable_to_select_image),
											registerActivity);
						}

					}
				} catch (OutOfMemoryError e) {
					System.out.println("out of bound");
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

			else {
				Toast.makeText(
						registerActivity,
						getResources().getString(
								R.string.toast_unable_to_selct_image),
						Toast.LENGTH_LONG).show();
			}

		} else if (requestCode == AndyConstants.TAKE_PHOTO) {

			if (uri != null) {
				// profileImageFilePath = uri.getPath();
				uri.getPath();
				registerActivity.setFbTag(AndyConstants.REGISTER_FRAGMENT_TAG);
				AppLog.Log(TAG, "Take photo on activity result");
				beginCrop(uri);
				// if (profileImageFilePath != null
				// && profileImageFilePath.length() > 0) {
				// File myFile = new File(profileImageFilePath);
				//
				// try {
				// if (profilePicBitmap != null)
				// profilePicBitmap.recycle();
				// profilePicBitmap = new ImageHelper().decodeFile(myFile);
				// } catch (OutOfMemoryError e) {
				// System.out.println("out of bound");
				// }
				// ivProfile.setImageBitmap(profilePicBitmap);
				// profileImageData = profileImageFilePath;
				// System.out.println(profileImageData);
				// } else {
				// Toast.makeText(
				// registerActivity,
				// registerActivity.getResources().getString(
				// R.string.toast_unable_to_selct_image),
				// Toast.LENGTH_LONG).show();
				// }

			} else {
				Toast.makeText(
						registerActivity,
						registerActivity.getResources().getString(
								R.string.toast_unable_to_selct_image),
						Toast.LENGTH_LONG).show();
			}
		} else if (requestCode == Crop.REQUEST_CROP) {
			AppLog.Log(TAG, "Crop photo on activity result");
			handleCrop(resultCode, data);
		} else {
			mSimpleFacebook.onActivityResult(registerActivity, requestCode,
					resultCode, data);
			if (mSimpleFacebook.isLogin()) {
				getFbProfile();
			} else {
				Toast.makeText(
						registerActivity,
						registerActivity.getResources().getString(
								R.string.toast_facebook_login_failed),
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void choosePhotoFromGallary() {

		// Intent intent = new Intent();
		// intent.setType("image/*");
		// intent.setAction(Intent.ACTION_GET_CONTENT);
		// intent.addCategory(Intent.CATEGORY_OPENABLE);
		Intent galleryIntent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		registerActivity
				.startActivityForResult(galleryIntent,
						AndyConstants.CHOOSE_PHOTO,
						AndyConstants.REGISTER_FRAGMENT_TAG);

	}

	private void takePhotoFromCamera() {
		Calendar cal = Calendar.getInstance();
		File file = new File(Environment.getExternalStorageDirectory(),
				(cal.getTimeInMillis() + ".jpg"));

		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {

			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		uri = Uri.fromFile(file);
		Intent cameraIntent = new Intent(
				android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
		registerActivity.startActivityForResult(cameraIntent,
				AndyConstants.TAKE_PHOTO, AndyConstants.REGISTER_FRAGMENT_TAG);
	}

	@Override
	public void onConnected(Bundle arg0) {
		AndyUtils.removeCustomProgressDialog();
		String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
		Person currentPerson = Plus.PeopleApi
				.getCurrentPerson(mGoogleApiClient);

		String personName = currentPerson.getDisplayName();
		String personPhoto = currentPerson.getImage().toString();

		btnGplus.setEnabled(false);
		btnFb.setEnabled(false);
		etRegisterEmail.setText(email);
		if (personName.contains(" ")) {
			String[] split = personName.split(" ");
			etRegisterFname.setText(split[0]);
			etRegisterLName.setText(split[1]);
		} else {
			etRegisterFname.setText(personName);
		}

		// etRegisterPassword.setEnabled(false);
		etRegisterPassword.setVisibility(View.GONE);
		if (!TextUtils.isEmpty(personPhoto)
				|| !personPhoto.equalsIgnoreCase("null")) {
			try {
				socialProPicUrl = new JSONObject(personPhoto).getString("url");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			aQuery.id(ivProfile).image(socialProPicUrl, profileImageOptions);
		} else {
			socialProPicUrl = null;
		}

		socialId = currentPerson.getId();
		loginType = AndyConstants.SOCIAL_GOOGLE;
		// etRegisterPassword.setEnabled(false);
		etRegisterPassword.setVisibility(View.GONE);
	}

	private void register(String type, String id) {

		Log.d("TYPe registre :: ", "" + getSelectedType());

		if (!AndyUtils.isNetworkAvailable(registerActivity)) {
			AndyUtils.showToast(
					getResources().getString(R.string.toast_no_internet),
					registerActivity);
			return;
		}

		AndyUtils.showCustomProgressDialog(registerActivity, "", getResources()
				.getString(R.string.progress_dialog_register), false);

		if (type.equals(AndyConstants.MANUAL)) {
			AppLog.Log(TAG, "Simple Register method");
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(AndyConstants.URL, AndyConstants.ServiceType.REGISTER);
			map.put(AndyConstants.Params.FIRSTNAME, etRegisterFname.getText()
					.toString());
			map.put(AndyConstants.Params.LAST_NAME, etRegisterLName.getText()
					.toString());
			map.put(AndyConstants.Params.EMAIL, etRegisterEmail.getText()
					.toString());
			map.put(AndyConstants.Params.PASSWORD, etRegisterPassword.getText()
					.toString());
			map.put(AndyConstants.Params.STATE, etRegisterState.getText()
					.toString());
			map.put(AndyConstants.Params.COUNTRY, etRegisterCountry.getText()
					.toString());
			map.put(AndyConstants.Params.CAR_MODEL, etRegisterCarModel
					.getText().toString());

			if (!TextUtils.isEmpty(profileImageData)) {
				map.put(AndyConstants.Params.PICTURE, profileImageData);
			}
			map.put(AndyConstants.Params.PHONE, tvCountryCode.getText()
					.toString().trim()
					+ etRegisterNumber.getText().toString());
			map.put(AndyConstants.Params.BIO, etRegisterBio.getText()
					.toString());
			map.put(AndyConstants.Params.ADDRESS, etRegisterAddress.getText()
					.toString());

			map.put(AndyConstants.Params.ZIPCODE, etRegisterZipcode.getText()
					.toString().trim());
			// map.put(AndyConstants.Params.TYPE,
			// String.valueOf(listType.get(selectedTypePostion).getId()));
			map.put(AndyConstants.Params.DEVICE_TYPE,
					AndyConstants.DEVICE_TYPE_ANDROID);
			map.put(AndyConstants.Params.DEVICE_TOKEN, new PreferenceHelper(
					registerActivity).getDeviceToken());
			// map.put(AndyConstants.Params.TAXI_MODEL,
			// etRegisterModel.getText()
			// .toString().trim());
			// map.put(AndyConstants.Params.TAXI_NUMBER, etRegisterTaxiNo
			// .getText().toString().trim());
			map.put(AndyConstants.Params.LOGIN_BY, AndyConstants.MANUAL);

			map.put(AndyConstants.Params.TYPE, getSelectedType());
			map.put(AndyConstants.Params.LICENSE, etLicensePlatNo.getText()
					.toString());
			map.put(AndyConstants.Params.DESCRIPTION, "");

			// gotoServiceFragment(map);

			new MultiPartRequester(registerActivity, map,
					AndyConstants.ServiceCode.REGISTER, this);
		} else {
			registerSoicial(id, type);
		}
	}

	// private String getSelectedType() {
	// ArrayList<Integer> listSelectedType = new ArrayList<Integer>();
	// for (int i = 0; i < listType.size(); i++) {
	// if (listType.get(i).isSelected)
	// listSelectedType.add(listType.get(i).getId());
	//
	// }
	// return TextUtils.join(",", listSelectedType);
	// }

	private void registerSoicial(String id, String loginType) {
		AppLog.Log(TAG, "Register social method");
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(AndyConstants.URL, AndyConstants.ServiceType.REGISTER);
		map.put(AndyConstants.Params.FIRSTNAME, etRegisterFname.getText()
				.toString());
		map.put(AndyConstants.Params.LAST_NAME, etRegisterLName.getText()
				.toString());
		map.put(AndyConstants.Params.ADDRESS, etRegisterAddress.getText()
				.toString());
		map.put(AndyConstants.Params.EMAIL, etRegisterEmail.getText()
				.toString());
		map.put(AndyConstants.Params.PHONE, tvCountryCode.getText().toString()
				.trim()
				+ etRegisterNumber.getText().toString());
		if (!TextUtils.isEmpty(profileImageData)) {
			map.put(AndyConstants.Params.PICTURE, profileImageData);
		}
		map.put(AndyConstants.Params.CAR_MODEL, etRegisterCarModel.getText()
				.toString());
		map.put(AndyConstants.Params.STATE, etRegisterState.getText()
				.toString());
		// map.put(AndyConstants.Params.TYPE,
		// String.valueOf(listType.get(selectedTypePostion).getId()));
		map.put(AndyConstants.Params.COUNTRY, etRegisterCountry.getText()
				.toString());
		map.put(AndyConstants.Params.BIO, etRegisterBio.getText().toString());
		map.put(AndyConstants.Params.DEVICE_TYPE,
				AndyConstants.DEVICE_TYPE_ANDROID);
		map.put(AndyConstants.Params.DEVICE_TOKEN, new PreferenceHelper(
				registerActivity).getDeviceToken());
		map.put(AndyConstants.Params.ZIPCODE, etRegisterZipcode.getText()
				.toString().trim());
		map.put(AndyConstants.Params.TYPE, getSelectedType());
		map.put(AndyConstants.Params.LICENSE, etLicensePlatNo.getText()
				.toString());
		map.put(AndyConstants.Params.DESCRIPTION, etDescription.getText()
				.toString());

		map.put(AndyConstants.Params.SOCIAL_UNIQUE_ID, id);
		map.put(AndyConstants.Params.LOGIN_BY, loginType);
		// gotoServiceFragment(map);
		new MultiPartRequester(registerActivity, map,
				AndyConstants.ServiceCode.REGISTER, this);

	}

	// private void gotoServiceFragment(HashMap<String, String> map) {
	// Bundle bundle = new Bundle();
	// bundle.putSerializable(AndyConstants.MAP, map);
	// ServiceFragment serviceFragment = new ServiceFragment();
	// serviceFragment.setArguments(bundle);
	// registerActivity.addFragment(serviceFragment, true,
	// AndyConstants.SERVICE_FRAGMENT_TAG, true);
	// }

	@Override
	public void onConnectionSuspended(int arg0) {
	}

	@Override
	public void onTaskCompleted(String response, int serviceCode) {
		switch (serviceCode) {
		// case AndyConstants.ServiceCode.GET_VEHICAL_TYPES:
		// AppLog.Log(TAG, "Vehicle types  " + response);
		// if (parseContent.isSuccess(response)) {
		// // parseContent.parseTypes(response, listType);
		// // adapter.notifyDataSetChanged();
		// }
		// AndyUtils.removeCustomProgressDialog();
		// break;

		case AndyConstants.ServiceCode.GET_VEHICAL_TYPES:
			if (parseContent.isSuccess(response)) {
				listType.clear();
				parseContent.parseTypes(response, listType);

				isGettingVehicalType = false;
				if (listType.size() > 0) {
					if (listType != null && listType.get(0) != null)
						listType.get(0).isSelected = true;
					typeAdapter.notifyDataSetChanged();
				}
			}
			AndyUtils.removeCustomProgressDialog();
			break;

		case AndyConstants.ServiceCode.REGISTER:
			AndyUtils.removeCustomProgressDialog();
			AppLog.Log(TAG, "Register response :" + response);
			if (parseContent.isSuccessRegister(response)) {
				AndyUtils.showToast(
						registerActivity.getResources().getString(
								R.string.toast_register_success),
						registerActivity);
				registerActivity.addFragment(new LoginFragment(), false,
						AndyConstants.LOGIN_FRAGMENT_TAG, true);
				// registerActivity.finish();
			} else {
				AndyUtils.showToast(
						registerActivity.getResources().getString(
								R.string.toast_register_failed),
						registerActivity);
			}

			break;

		default:
			break;
		}

	}

	private String getRealPathFromURI(Uri contentURI) {
		String result;
		Cursor cursor = registerActivity.getContentResolver().query(contentURI,
				null, null, null, null);

		if (cursor == null) { // Source is Dropbox or other similar local file
								// path
			result = contentURI.getPath();
		} else {
			cursor.moveToFirst();
			try {
				int idx = cursor
						.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
				result = cursor.getString(idx);
			} catch (Exception e) {
				AndyUtils
						.showToast(
								getResources().getString(
										R.string.text_error_get_image),
								registerActivity);
				result = "";
			}
			cursor.close();
		}
		return result;
	}

	@Override
	public void onStop() {
		super.onStop();
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		AndyUtils.removeCustomProgressDialog();
	}

	// private void getVehicalTypes() {
	// AndyUtils.showCustomProgressDialog(registerActivity, "", getResources()
	// .getString(R.string.progress_getting_types), false);
	// HashMap<String, String> map = new HashMap<String, String>();
	// map.put(AndyConstants.URL, AndyConstants.ServiceType.GET_VEHICAL_TYPES);
	// AppLog.Log(TAG, AndyConstants.URL);
	// // new HttpRequester(registerActivity, map,
	// // AndyConstants.ServiceCode.GET_VEHICAL_TYPES, true, this);
	//
	// requestQueue.add(new VolleyHttpRequest(Method.GET, map,
	// AndyConstants.ServiceCode.GET_VEHICAL_TYPES, this, this));
	// }

	@Override
	public void onResume() {
		super.onResume();
		registerActivity.currentFragment = AndyConstants.REGISTER_FRAGMENT_TAG;
		mSimpleFacebook = SimpleFacebook.getInstance(registerActivity);
	}

	private void beginCrop(Uri source) {
		// Uri outputUri = Uri.fromFile(new File(registerActivity.getCacheDir(),
		// "cropped"));
		Uri outputUri = Uri.fromFile(new File(Environment
				.getExternalStorageDirectory(), (Calendar.getInstance()
				.getTimeInMillis() + ".jpg")));
		new Crop(source).output(outputUri).asSquare().start(registerActivity);
	}

	private void handleCrop(int resultCode, Intent result) {
		if (resultCode == RegisterActivity.RESULT_OK) {
			AppLog.Log(TAG, "Handle crop");
			profileImageData = getRealPathFromURI(Crop.getOutput(result));
			ivProfile.setImageURI(Crop.getOutput(result));
		} else if (resultCode == Crop.RESULT_ERROR) {
			Toast.makeText(registerActivity,
					Crop.getError(result).getMessage(), Toast.LENGTH_SHORT)
					.show();
		}
	}
}
