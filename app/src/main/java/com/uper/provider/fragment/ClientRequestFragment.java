package com.uper.provider.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.uper.provider.MapActivity;
import com.uper.provider.R;
import com.uper.provider.adapter.SubSubCategoryListAdapter;
import com.uper.provider.base.BaseMapFragment;
import com.uper.provider.locationupdate.LocationHelper;
import com.uper.provider.locationupdate.LocationHelper.OnLocationReceived;
import com.uper.provider.model.BeanRoute;
import com.uper.provider.model.BeanStep;
import com.uper.provider.model.RequestDetail;
import com.uper.provider.parse.AsyncTaskCompleteListener;
import com.uper.provider.parse.HttpRequester;
import com.uper.provider.utills.AndyConstants;
import com.uper.provider.utills.AndyUtils;
import com.uper.provider.utills.AppLog;
import com.uper.provider.widget.MyFontButton;
import com.uper.provider.widget.MyFontTextView;

public class ClientRequestFragment extends BaseMapFragment implements
		AsyncTaskCompleteListener, OnLocationReceived , OnMapReadyCallback{
	private GoogleMap mMap;
	private final String TAG = "ClientRequestFragment";
	private static LinearLayout llAcceptReject;
	private static View llUserDetailView;
	private MyFontButton btnClientAccept, btnClientReject,
			btnClientNegotiation;
	private boolean isContinueRequest, isAccepted = false,
			isApprovedCheck = true, loaded;
	private Timer timer;
	private RequestDetail requestDetail;
	private Marker markerDriverLocation, markerClientLocation;
	private static SeekbarTimer seekbarTimer;
	private static MyFontButton btnClientReqRemainTime;
	private Location location;
	private LocationHelper locationHelper;
	private MyFontTextView tvClientName;// , tvClientPhoneNumber;
	private RatingBar tvClientRating;
	private ImageView ivClientProfilePicture;
	private AQuery aQuery;
	private newRequestReciever requestReciever;
	private View clientRequestView;
	//private MapView mMapView;
	private Bundle mBundle;
	private Dialog mDialog;
	private int soundid, counter, streamId;
	private static SoundPool soundPool;
	private static Button btnGoOffline;
	private RelativeLayout relMap;
	private LinearLayout linearOffline;
	private TextView tvDestinationAddress, tvSourceAddress;
	private String nPrice, clientPrice = "0";
	private Button buttonMoreInfo;

	private BeanRoute routeDest, routeClient;
	private ArrayList<LatLng> pointsDest;

	LatLng latSource, latDest;
	private PolylineOptions lineOptionsDest;
	private Polyline polyLineDest;
	private Marker markerDestination;

	private ImageOptions imageOptions;
	private MyFontTextView tvClientNegotiationPrice, tvTotalPrice;
	private LinearLayout llPrice;
	private SupportMapFragment mapFragment;
	boolean isRequestArrived = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mBundle = savedInstanceState;
		IntentFilter filter = new IntentFilter(AndyConstants.NEW_REQUEST);
		requestReciever = new newRequestReciever();
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				requestReciever, filter);



		counter = 0;
		soundPool = new SoundPool(5, AudioManager.STREAM_ALARM, 100);
		soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
			@Override
			public void onLoadComplete(SoundPool soundPool, int sampleId,
					int status) {
				loaded = true;
			}
		});
		soundid = soundPool.load(mapActivity, R.raw.beep, 1);
	}

	/*
	 * if(requestId == AndyConstants.NO_REQUEST &&
	 * AndyConstants.CONFORMED_WLAKER ==
	 * Integer.parseInt(preferenceHelper.getUserId())) { checkRequestStatus(); }
	 * else { getAllRequests(); }
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		clientRequestView = inflater.inflate(R.layout.fragment_client_requests,
				container, false);

		mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragmentMap);
		try {
			MapsInitializer.initialize(getActivity());
		} catch (Exception e) {

		}

		aQuery = new AQuery(mapActivity);

		llAcceptReject = (LinearLayout) clientRequestView
				.findViewById(R.id.llAcceptReject);
		llUserDetailView = (View) clientRequestView
				.findViewById(R.id.clientDetailView);
		btnClientAccept = (MyFontButton) clientRequestView
				.findViewById(R.id.btnClientAccept);
		btnClientReject = (MyFontButton) clientRequestView
				.findViewById(R.id.btnClientReject);
		btnClientNegotiation = (MyFontButton) clientRequestView
				.findViewById(R.id.btnClientNegotiation);
		btnClientNegotiation.setOnClickListener(this);
		tvClientNegotiationPrice = (MyFontTextView) clientRequestView
				.findViewById(R.id.tvClientNegotiationPrice);
		tvTotalPrice = (MyFontTextView) clientRequestView
				.findViewById(R.id.tvTotalPrice);
		llPrice = (LinearLayout) clientRequestView.findViewById(R.id.llPrice);
		tvDestinationAddress = (TextView) clientRequestView
				.findViewById(R.id.tvDestinationAddress);
		tvSourceAddress = (TextView) clientRequestView
				.findViewById(R.id.tvSourceAddress);

		linearOffline = (LinearLayout) clientRequestView
				.findViewById(R.id.linearOffline);
		// pbTimeLeft = (ProgressBar) clientRequestView
		// .findViewById(R.id.pbClientReqTime);
		// rlTimeLeft = (RelativeLayout) clientRequestView
		// .findViewById(R.id.rlClientReqTimeLeft);
		btnClientReqRemainTime = (MyFontButton) clientRequestView
				.findViewById(R.id.btnClientReqRemainTime);

		btnClientReqRemainTime.setVisibility(View.GONE);
		tvClientName = (MyFontTextView) clientRequestView
				.findViewById(R.id.tvClientName);
		// tvClientPhoneNumber = (MyFontTextView) clientRequestView
		// .findViewById(R.id.tvClientNumber);

		tvClientRating = (RatingBar) clientRequestView
				.findViewById(R.id.tvClientRating);

		ivClientProfilePicture = (ImageView) clientRequestView
				.findViewById(R.id.ivClientImage);

		buttonMoreInfo = (Button) clientRequestView
				.findViewById(R.id.buttonMoreInfo);
		buttonMoreInfo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showClientDetailDoalog();
			}
		});

		btnClientAccept.setOnClickListener(this);
		btnClientReject.setOnClickListener(this);
		clientRequestView.findViewById(R.id.btnMyLocation).setOnClickListener(
				this);
		btnGoOffline = (Button) clientRequestView.findViewById(R.id.btnOffline);
		relMap = (RelativeLayout) clientRequestView.findViewById(R.id.relMap);
		linearOffline.setVisibility(View.GONE);
		relMap.setVisibility(View.VISIBLE);
		btnGoOffline.setOnClickListener(this);
		btnGoOffline.setSelected(true);

		return clientRequestView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		aQuery = new AQuery(mapActivity);
		//mMapView = (MapView) clientRequestView.findViewById(R.id.clientReqMap);
	//	mMapView.onCreate(mBundle);

		setUpMap();
		locationHelper = new LocationHelper(getActivity());
		locationHelper.setLocationReceivedLister(this);
		locationHelper.onStart();
		checkState();

	}

	private void showClientDetailDoalog() {
		final Dialog mDialog = new Dialog(mapActivity,
				android.R.style.Theme_Translucent_NoTitleBar);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setContentView(R.layout.dialog_client_moreinfo);

		imageOptions = new ImageOptions();
		imageOptions.memCache = true;
		imageOptions.fileCache = true;
		imageOptions.targetWidth = 200;
		imageOptions.fallback = R.drawable.user;

		mDialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		TextView tvDestination = (TextView) mDialog
				.findViewById(R.id.tvDestination);

		TextView tvSource = (TextView) mDialog.findViewById(R.id.tvSource);

		ListView listviewItemSummery = (ListView) mDialog
				.findViewById(R.id.listviewItemSummery);
		SubSubCategoryListAdapter adpt = new SubSubCategoryListAdapter(
				mapActivity, requestDetail.getSubTypeList());
		listviewItemSummery.setAdapter(adpt);

		TextView tvNegotiationPrice = (TextView) mDialog
				.findViewById(R.id.tvNegotiationPrice);
		tvNegotiationPrice.setText("Negotiation Price : "
				+ requestDetail.getClientAmount());

		CircularImageView ivProfile = (CircularImageView) mDialog
				.findViewById(R.id.ivProfile);

		TextView tvDescription = (TextView) mDialog
				.findViewById(R.id.tvDescription);
		tvDescription
				.setText("Description : " + requestDetail.getDescrpition());

		aQuery.id(ivProfile).progress(R.id.pBar)
				.image(requestDetail.getPicture(), imageOptions);

		mDialog.setCancelable(true);
		mDialog.setCanceledOnTouchOutside(true);

		String destination = requestDetail.getDestinationAddress();
		destination = destination.replace("\n", "");
		tvDestination.setText(getResources().getString(R.string.text_to) + " "
				+ destination);

		String source = requestDetail.getSourceAddress();
		source = source.replace("\n", "");
		tvSource.setText(getResources().getString(R.string.text_from) + " "
				+ source);

		MyFontTextView tvClose = (MyFontTextView) mDialog
				.findViewById(R.id.tvClose);
		tvClose.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mDialog.dismiss();
			}
		});

		mDialog.show();
	}

	private void addMarker() {
		if (mMap == null) {
			setUpMap();
			return;
		}

	}

	public void showLocationOffDialog() {

		AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(mapActivity);
		gpsBuilder.setCancelable(false);
		gpsBuilder
				.setTitle(getString(R.string.dialog_no_location_service_title))
				.setMessage(getString(R.string.dialog_no_location_service))
				.setPositiveButton(
						getString(R.string.dialog_enable_location_service),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// continue with delete
								dialog.dismiss();
								Intent viewIntent = new Intent(
										Settings.ACTION_LOCATION_SOURCE_SETTINGS);
								startActivity(viewIntent);

							}
						})

				.setNegativeButton(getString(R.string.dialog_exit),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// do nothing
								dialog.dismiss();
								mapActivity.finish();
							}
						});
		gpsBuilder.create();
		gpsBuilder.show();
	}

	private void setUpMap() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null)
		{
			//mMap = ((MapView) clientRequestView.findViewById(R.id.clientReqMap)).getMapAsync(this);
			mapFragment.getMapAsync(this);
		}


				}

	private void showNegotiationDialog() {
		final Dialog mDialog = new Dialog(mapActivity,
				android.R.style.Theme_Translucent_NoTitleBar);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setContentView(R.layout.dialog_negotiation_price);
		mDialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		mDialog.setCancelable(false);

		mDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				// Prevent dialog close on back press button
				return keyCode == KeyEvent.KEYCODE_BACK;
			}
		});

		final EditText etNagotiationPrice = (EditText) mDialog
				.findViewById(R.id.etNagotiationPrice);

		TextView tvClientNagotiationPrice = (TextView) mDialog
				.findViewById(R.id.tvClientNagotiationPrice);

		tvClientNagotiationPrice.setText(mapActivity.getResources().getString(
				R.string.client_price)
				+ " "
				+ getResources().getString(R.string.currency)
				+ requestDetail.getClientAmount());

		Button btnSubmmit = (Button) mDialog.findViewById(R.id.btnSubmmit);
		btnSubmmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (TextUtils.isEmpty(etNagotiationPrice.getText().toString())) {
					AndyUtils.showToast(
							getResources().getString(
									R.string.toast_enter_neg_price),
							mapActivity);
					return;
				} else {
					nPrice = etNagotiationPrice.getText().toString();
					acceptNagotiation(nPrice);
					mDialog.dismiss();
				}
			}

		});
		Button btnCancel = (Button) mDialog.findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mDialog.dismiss();
			}
		});

		mDialog.show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.btnClientNegotiation:
			mapActivity.clearAll();
			isAccepted = false;
			showNegotiationDialog();
			// cancelSeekbarTimer();
			break;

		case R.id.btnClientAccept:
			stopCheckingUpcomingRequests();
			mapActivity.clearAll();
			isAccepted = true;
			cancelSeekbarTimer();
			respondRequest(1);

			break;
		case R.id.btnClientReject:
			mapActivity.clearAll();
			isAccepted = false;
			cancelSeekbarTimer();
			respondRequest(0);
			break;
		case R.id.btnMyLocation:
			if (location != null) {
				LatLng latLng = new LatLng(location.getLatitude(),
						location.getLongitude());
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
						latLng, 18);
				mMap.animateCamera(cameraUpdate);
			}
			break;
		case R.id.btnOffline:
			mapActivity.clearAll();
			changeState();
			break;
		default:
			break;
		}
	}

	private void acceptNagotiation(String price) {
		if (!AndyUtils.isNetworkAvailable(mapActivity)) {
			AndyUtils.showToast(
					getResources().getString(R.string.toast_no_internet),
					mapActivity);
			return;
		}

		AndyUtils.showCustomProgressDialog(mapActivity, "", getResources()
				.getString(R.string.progress_respond_request), false);

		HashMap<String, String> map = new HashMap<String, String>();
		map.put(AndyConstants.URL, AndyConstants.ServiceType.NEGOTIATION);
		map.put(AndyConstants.Params.ID, preferenceHelper.getUserId());
		map.put(AndyConstants.Params.REQUEST_ID,
				String.valueOf(requestDetail.getRequestId()));
		map.put(AndyConstants.Params.AMOUNT, price);
		map.put(AndyConstants.Params.TOKEN, preferenceHelper.getSessionToken());
		new HttpRequester(mapActivity, map,
				AndyConstants.ServiceCode.NEGOTIATION, this);

	}

	@Override
	public void onResume() {
		super.onResume();
		//mMapView.onResume();
		if (btnGoOffline.isSelected()) {
			if (preferenceHelper.getRequestId() == AndyConstants.NO_REQUEST) {
				startCheckingUpcomingRequests();
			}
		}
		mapActivity.setActionBarTitle(getString(R.string.action_app_name));

	}

	@Override
	public void onPause() {
		super.onPause();
		if (preferenceHelper.getRequestId() == AndyConstants.NO_REQUEST) {
			stopCheckingUpcomingRequests();
		}
		//mMapView.onPause();
	}

	@Override
	public void onDestroy() {
		//mMapView.onDestroy();
		stopCheckingUpcomingRequests();
		cancelSeekbarTimer();
		AndyUtils.removeCustomProgressDialog();
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(
				requestReciever);
		super.onDestroy();
	}

	// public void openApprovedDialog() {
	// mDialog = new Dialog(mapActivity,
	// android.R.style.Theme_Translucent_NoTitleBar);
	// mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	//
	// mDialog.getWindow().setBackgroundDrawable(
	// new ColorDrawable(android.graphics.Color.TRANSPARENT));
	// mDialog.setContentView(R.layout.provider_approve_dialog);
	// mDialog.setCancelable(false);
	// tvApprovedClose = (MyFontTextView) mDialog
	// .findViewById(R.id.tvApprovedClose);
	// tvApprovedClose.setOnClickListener(new OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// mDialog.dismiss();
	// mapActivity.finish();
	// }
	// });
	// mDialog.show();
	// }

	@Override
	public void onTaskCompleted(String response, int serviceCode) {

		switch (serviceCode) {
		case AndyConstants.ServiceCode.NEGOTIATION:

			AndyUtils.removeCustomProgressDialog();
			AppLog.Log("NEGOTIATION RESPONSE :: ", response);
			if (parseContent.isSuccess(response)) {
				btnClientAccept.setEnabled(false);
				btnClientReject.setEnabled(false);
				btnClientNegotiation.setEnabled(false);
				btnClientAccept.setBackgroundColor(getResources().getColor(
						R.color.transparent_gray));
				btnClientReject.setBackgroundColor(getResources().getColor(
						R.color.gray));
				btnClientNegotiation.setBackgroundColor(getResources()
						.getColor(R.color.gray));
			}

			break;

		case AndyConstants.ServiceCode.GET_ALL_REQUEST:
			AndyUtils.removeCustomProgressDialog();
			AppLog.Log(TAG, "getAllRequests Response :" + response);
			if (!parseContent.parseIsApproved(response)) {
				if (isApprovedCheck) {
					mapActivity.openApprovedDialog();
					isApprovedCheck = false;
					return;
				}
			} else if (mDialog != null && mDialog.isShowing()) {
				mDialog.dismiss();
				isApprovedCheck = true;
			}
			if (!parseContent.isSuccess(response)) {
				return;
			}
			requestDetail = parseContent.parseAllRequests(response);
			if (requestDetail == null
					&& btnClientReqRemainTime.getVisibility() == View.VISIBLE) {
				setComponentInvisible();
				mMap.clear();
				cancelSeekbarTimer();
				startCheckingUpcomingRequests();
				return;
			}

			if (requestDetail != null && mMap != null) {
				if (preferenceHelper.getUserId().equals(
						requestDetail.getClientRequestAccepted())) {
					Log.d("conform the walker ::: and go job", "okoko");

					isAccepted = true;
					stopCheckingUpcomingRequests();
					cancelSeekbarTimer();
					setComponentInvisible();

					preferenceHelper.putRequestId(requestDetail.getRequestId());

					Log.d("REQUEST ID :::::::::::::: ",preferenceHelper.getRequestId()+"");


					JobFragment jobFragment = new JobFragment();
					Bundle bundle = new Bundle();
					bundle.putInt(AndyConstants.JOB_STATUS,
							AndyConstants.IS_WALKER_STARTED);
					bundle.putSerializable(AndyConstants.REQUEST_DETAIL,
							requestDetail);
					jobFragment.setArguments(bundle);
					if (this.isVisible())
						mapActivity.addFragment(jobFragment, false,
								AndyConstants.JOB_FRGAMENT_TAG, true);
					return;
				} else if (btnClientReqRemainTime.getVisibility() == View.VISIBLE) {
					return;
				}
				try {
					setComponentVisible();
					btnClientReqRemainTime.setText(""
							+ requestDetail.getTimeLeft());

					tvClientName.setText(requestDetail.getClientName());

					tvTotalPrice.setText(getResources().getString(
							R.string.text_total_price)
							+ " "
							+ getResources().getString(R.string.currency)
							+ requestDetail.getTotalAmount());
					tvClientNegotiationPrice.setText(getResources().getString(
							R.string.text_customer_negotiated_price)
							+ " "
							+ getResources().getString(R.string.currency)
							+ requestDetail.getClientAmount());

					String destination = requestDetail.getDestinationAddress();
					destination = destination.replace("\n", "");
					tvDestinationAddress.setText(getResources().getString(
							R.string.text_to)
							+ " " + destination);

					String source = requestDetail.getSourceAddress();
					source = source.replace("\n", "");
					tvSourceAddress.setText(getResources().getString(
							R.string.text_from)
							+ " " + source);

					latSource = new LatLng(Double.parseDouble(requestDetail
							.getClientLatitude()),
							Double.parseDouble(requestDetail
									.getClientLongitude()));

					latDest = new LatLng(requestDetail.getD_lat(),
							requestDetail.getD_long());

					if (latSource == null || latDest == null) {
					} else {
						drawPath(latSource, latDest);
					}

					clientPrice = requestDetail.getClientAmount();

					Log.d("CLIENT PRICE ::::::::::::: ",
							"" + requestDetail.getClientAmount());

					if (requestDetail.getClientRating() != 0) {
						tvClientRating.setRating(requestDetail
								.getClientRating());
						Log.i("Rating", "" + requestDetail.getClientRating());
					}
					if (TextUtils.isEmpty(requestDetail.getClientProfile())) {
						aQuery.id(ivClientProfilePicture).progress(R.id.pBar)
								.image(R.drawable.user);
					} else {
						aQuery.id(ivClientProfilePicture).progress(R.id.pBar)
								.image(requestDetail.getClientProfile());
					}

					if (markerClientLocation == null) {
						markerClientLocation = mMap
								.addMarker(new MarkerOptions()
										.position(
												new LatLng(
														Double.parseDouble(requestDetail
																.getClientLatitude()),
														Double.parseDouble(requestDetail
																.getClientLongitude())))
										.icon(BitmapDescriptorFactory
												.fromResource(R.drawable.pin_client))
										.title(mapActivity
												.getResources()
												.getString(
														R.string.client_location)));
					} else {
						markerClientLocation.setPosition(new LatLng(
								Double.parseDouble(requestDetail
										.getClientLatitude()), Double
										.parseDouble(requestDetail
												.getClientLongitude())));
					}

					if (seekbarTimer == null) {
						seekbarTimer = new SeekbarTimer(
								requestDetail.getTimeLeft() * 1000, 1000);
						seekbarTimer.start();
					}

					// if client accept the negotiation price

					Log.d("conform walker :: ",
							"" + requestDetail.getClientRequestAccepted() + " "
									+ preferenceHelper.getUserId());

					if (preferenceHelper.getUserId().equals(
							requestDetail.getClientRequestAccepted())) {
						Log.d("conform the walker ::: and go job", "okoko");

						isAccepted = true;
						stopCheckingUpcomingRequests();
						cancelSeekbarTimer();
						setComponentInvisible();

						preferenceHelper.putRequestId(requestDetail
								.getRequestId());

						Log.d("Now accepted REquest Id is :: ", ""
								+ preferenceHelper.getRequestId());

						JobFragment jobFragment = new JobFragment();
						Bundle bundle = new Bundle();
						bundle.putInt(AndyConstants.JOB_STATUS,
								AndyConstants.IS_WALKER_STARTED);
						bundle.putSerializable(AndyConstants.REQUEST_DETAIL,
								requestDetail);
						jobFragment.setArguments(bundle);
						if (this.isVisible())
							mapActivity.addFragment(jobFragment, false,
									AndyConstants.JOB_FRGAMENT_TAG, true);

					}

				} catch (Exception e) {
					e.printStackTrace();

				}

			} else {

				cancelSeekbarTimer();
				setComponentInvisible();
				preferenceHelper.putRequestId(AndyConstants.NO_REQUEST);

			}
			break;
		case AndyConstants.ServiceCode.CHECK_STATE:
		case AndyConstants.ServiceCode.TOGGLE_STATE:
			AndyUtils.removeCustomProgressDialog();
			preferenceHelper.putIsActive(false);
			preferenceHelper.putDriverOffline(false);
			if (!parseContent.isSuccess(response)) {
				return;
			}
			AppLog.Log("TAG", "toggle state:" + response);
			if (parseContent.parseAvaibilty(response)) {
				updateButtonUi(true);
				if (preferenceHelper.getRequestId() == AndyConstants.NO_REQUEST) {
					startCheckingUpcomingRequests();
				}

			} else {
				stopCheckingUpcomingRequests();
				updateButtonUi(false);
			}

			break;
		case AndyConstants.ServiceCode.RESPOND_REQUEST:
			AppLog.Log(TAG, "respond Request Response :" + response);
			removeNotification();
			AndyUtils.removeCustomProgressDialog();
			if (parseContent.isSuccess(response)) {

				if (isAccepted) {

					preferenceHelper.putRequestId(requestDetail.getRequestId());
					JobFragment jobFragment = new JobFragment();
					Bundle bundle = new Bundle();
					bundle.putInt(AndyConstants.JOB_STATUS,
							AndyConstants.IS_WALKER_STARTED);
					bundle.putSerializable(AndyConstants.REQUEST_DETAIL,
							requestDetail);
					jobFragment.setArguments(bundle);
					if (this.isVisible())
						mapActivity.addFragment(jobFragment, false,
								AndyConstants.JOB_FRGAMENT_TAG, true);

				} else {
					cancelSeekbarTimer();
					setComponentInvisible();
					if (markerClientLocation != null
							&& markerClientLocation.isVisible()) {
						markerClientLocation.remove();
						markerClientLocation = null;
					}
					preferenceHelper.putRequestId(AndyConstants.NO_REQUEST);
					startCheckingUpcomingRequests();
				}
			} else {
				setComponentInvisible();
			}
			break;

		case AndyConstants.ServiceCode.DRAW_PATH:
			if (!TextUtils.isEmpty(response)) {
				routeDest = new BeanRoute();
				parseContent.parseRoute(response, routeDest);

				final ArrayList<BeanStep> step = routeDest.getListStep();
				System.out.println("step size=====> " + step.size());
				pointsDest = new ArrayList<LatLng>();
				lineOptionsDest = new PolylineOptions();

				for (int i = 0; i < step.size(); i++) {
					List<LatLng> path = step.get(i).getListPoints();
					System.out.println("step =====> " + i + " and "
							+ path.size());
					pointsDest.addAll(path);
				}
				if (polyLineDest != null)
					polyLineDest.remove();
				lineOptionsDest.addAll(pointsDest);
				lineOptionsDest.width(15);
				lineOptionsDest.geodesic(true);
				lineOptionsDest.color(mapActivity.getResources().getColor(
						R.color.color_blue)); // #00008B rgb(0,0,139)

				if (lineOptionsDest != null && mMap != null) {
					polyLineDest = mMap.addPolyline(lineOptionsDest);
					boundLatLang();
					// LatLngBounds.Builder bld = new LatLngBounds.Builder();
					//
					// bld.include(markerClientLocation.getPosition());
					// bld.include(markerDestination.getPosition());
					// LatLngBounds latLngBounds = bld.build();
					// googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
					// latLngBounds, 15));
				}
			}
			break;
		default:
			break;

		}
	}

	private void drawPath(LatLng source, LatLng destination) {
		if (source == null || destination == null) {
			return;
		}
		if (destination.latitude != 0) {

			setDestinationMarker(destination);
			boundLatLang();
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(AndyConstants.URL,
					"http://maps.googleapis.com/maps/api/directions/json?origin="
							+ source.latitude + "," + source.longitude
							+ "&destination=" + destination.latitude + ","
							+ destination.longitude + "&sensor=false");
			new HttpRequester(mapActivity, map,
					AndyConstants.ServiceCode.DRAW_PATH, true, this);
		}
	}

	private void boundLatLang() {

		try {
			if (markerDriverLocation != null && markerClientLocation != null
					&& markerDestination != null) {
				LatLngBounds.Builder bld = new LatLngBounds.Builder();
				bld.include(new LatLng(
						markerDriverLocation.getPosition().latitude,
						markerDriverLocation.getPosition().longitude));
				bld.include(new LatLng(
						markerClientLocation.getPosition().latitude,
						markerClientLocation.getPosition().longitude));
				bld.include(new LatLng(
						markerDestination.getPosition().latitude,
						markerDestination.getPosition().longitude));
				LatLngBounds latLngBounds = bld.build();

				mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(
						latLngBounds, 50));
			} else if (markerDriverLocation != null
					&& markerClientLocation != null) {
				LatLngBounds.Builder bld = new LatLngBounds.Builder();
				bld.include(new LatLng(
						markerDriverLocation.getPosition().latitude,
						markerDriverLocation.getPosition().longitude));
				bld.include(new LatLng(
						markerClientLocation.getPosition().latitude,
						markerClientLocation.getPosition().longitude));
				LatLngBounds latLngBounds = bld.build();

				mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(
						latLngBounds, 100));
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	private void setDestinationMarker(LatLng latLng) {
		if (latLng != null) {
			if (mMap != null && this.isVisible()) {
				if (markerDestination == null) {
					MarkerOptions opt = new MarkerOptions();
					opt.position(latLng);
					opt.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.pin_client_destination));
					opt.title(getString(R.string.text_destination));
					markerDestination = mMap.addMarker(opt);
				} else {
					markerDestination.setPosition(latLng);
				}
			}
		}
	}

	private class SeekbarTimer extends CountDownTimer {

		public SeekbarTimer(long startTime, long interval) {
			super(startTime, interval);
			// pbTimeLeft.setProgressDrawable(getResources().getDrawable(
			// R.drawable.customprogress));
		}

		@Override
		public void onFinish() {
			if (seekbarTimer == null) {
				return;
			}
			AndyUtils.showToast(
					mapActivity.getResources().getString(
							R.string.toast_time_over), mapActivity);
			setComponentInvisible();
			preferenceHelper.clearRequestData();
			if (markerClientLocation != null
					&& markerClientLocation.isVisible()) {
				markerClientLocation.remove();
			}
			removeNotification();
			startCheckingUpcomingRequests();
			this.cancel();
			seekbarTimer = null;

		}

		@Override
		public void onTick(long millisUntilFinished) {
			int time = (int) (millisUntilFinished / 1000);

			if (!isVisible()) {
				return;
			}
			if (preferenceHelper.getSoundAvailability()) {
				if (time <= 15) {
					// AppLog.Log("ClientRequest Timer Beep", "Beep started");
					if (loaded) {
						streamId = soundPool.play(soundid, 1, 1, 0, 0, 1);
						counter++;
					}

				}
			}

			btnClientReqRemainTime.setText("" + time);
			// pbTimeLeft.setProgress(time);
			// if (time <= 5) {
			// pbTimeLeft.setProgressDrawable(getResources().getDrawable(
			// R.drawable.customprogressred));
			// }

		}
	}

	// if status = 1 then accept if 0 then reject
	private void respondRequest(int status) {
		if (!AndyUtils.isNetworkAvailable(mapActivity)) {
			AndyUtils.showToast(
					getResources().getString(R.string.toast_no_internet),
					mapActivity);
			return;
		}

		AndyUtils.showCustomProgressDialog(mapActivity, "", getResources()
				.getString(R.string.progress_respond_request), false);

		HashMap<String, String> map = new HashMap<String, String>();
		map.put(AndyConstants.URL, AndyConstants.ServiceType.RESPOND_REQUESTS);
		map.put(AndyConstants.Params.ID, preferenceHelper.getUserId());

		map.put(AndyConstants.Params.REQUEST_ID,
				String.valueOf(requestDetail.getRequestId()));
		map.put(AndyConstants.Params.TOKEN, preferenceHelper.getSessionToken());
		map.put(AndyConstants.Params.ACCEPTED, String.valueOf(status));
		new HttpRequester(mapActivity, map,
				AndyConstants.ServiceCode.RESPOND_REQUEST, this);
	}

	public void checkRequestStatus() {
		if (!AndyUtils.isNetworkAvailable(mapActivity)) {
			AndyUtils.showToast(
					getResources().getString(R.string.toast_no_internet),
					mapActivity);
			return;
		}
		AndyUtils.showCustomProgressDialog(mapActivity, "", getResources()
				.getString(R.string.progress_dialog_request), false);
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(AndyConstants.URL,
				AndyConstants.ServiceType.CHECK_REQUEST_STATUS
						+ AndyConstants.Params.ID + "="
						+ preferenceHelper.getUserId() + "&"
						+ AndyConstants.Params.TOKEN + "="
						+ preferenceHelper.getSessionToken() + "&"
						+ AndyConstants.Params.REQUEST_ID + "="
						+ preferenceHelper.getRequestId());
		new HttpRequester(mapActivity, map,
				AndyConstants.ServiceCode.CHECK_REQUEST_STATUS, true, this);
	}

	public void getAllRequests() {
		if (!AndyUtils.isNetworkAvailable(mapActivity)) {
			return;
		}

		HashMap<String, String> map = new HashMap<String, String>();
		map.put(AndyConstants.URL,
				AndyConstants.ServiceType.GET_ALL_REQUESTS
						+ AndyConstants.Params.ID + "="
						+ preferenceHelper.getUserId() + "&"
						+ AndyConstants.Params.TOKEN + "="
						+ preferenceHelper.getSessionToken());
		Log.d("tag",
				"URL=" + AndyConstants.ServiceType.GET_ALL_REQUESTS
						+ AndyConstants.Params.ID + "="
						+ preferenceHelper.getUserId() + "&"
						+ AndyConstants.Params.TOKEN + "="
						+ preferenceHelper.getSessionToken());
		new HttpRequester(mapActivity, map,
				AndyConstants.ServiceCode.GET_ALL_REQUEST, true, this);
	}

	private class TimerRequestStatus extends TimerTask {
		@Override
		public void run() {
			if (isContinueRequest) {
				getAllRequests();
			}
		}
	}

	private void startCheckingUpcomingRequests() {
		AppLog.Log(TAG, "start checking upcomingRequests");
		isContinueRequest = true;
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerRequestStatus(),
				AndyConstants.DELAY, AndyConstants.TIME_SCHEDULE);
	}

	private void stopCheckingUpcomingRequests() {
		AppLog.Log(TAG, "stop checking upcomingRequests");
		isContinueRequest = false;
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

	private void removeNotification() {
		try {
			NotificationManager manager = (NotificationManager) mapActivity
					.getSystemService(MapActivity.NOTIFICATION_SERVICE);
			manager.cancel(AndyConstants.NOTIFICATION_ID);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onLocationReceived(LatLng latlong) {
		if (latlong != null) {
			if (mMap != null) {
				if (markerDriverLocation == null) {
					markerDriverLocation = mMap.addMarker(new MarkerOptions()
							.position(
									new LatLng(latlong.latitude,
											latlong.longitude))
							.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.pin_driver))
							.title(mapActivity.getResources().getString(
									R.string.my_location)));
					mMap.animateCamera(CameraUpdateFactory
							.newLatLngZoom(new LatLng(latlong.latitude,
									latlong.longitude), 10));
				} else {
					markerDriverLocation.setPosition(new LatLng(
							latlong.latitude, latlong.longitude));
				}
			}
		}

	}

	public void setComponentVisible() {
		btnGoOffline.setVisibility(View.GONE);
		llAcceptReject.setVisibility(View.VISIBLE);
		btnClientReqRemainTime.setVisibility(View.VISIBLE);
		llUserDetailView.setVisibility(View.VISIBLE);
		llPrice.setVisibility(View.VISIBLE);
		tvDestinationAddress.setVisibility(View.VISIBLE);
		tvSourceAddress.setVisibility(View.VISIBLE);
		// getAddressFromLocation(
		// Double.parseDouble(requestDetail.getClientLatitude()),
		// (Double.parseDouble(requestDetail.getClientLongitude())),
		// tvDestinationAddress);
	}

	public void setComponentInvisible() {
		btnGoOffline.setVisibility(View.VISIBLE);
		llAcceptReject.setVisibility(View.GONE);
		btnClientReqRemainTime.setVisibility(View.GONE);
		llUserDetailView.setVisibility(View.GONE);
		tvDestinationAddress.setVisibility(View.GONE);
		llPrice.setVisibility(View.GONE);
		tvSourceAddress.setVisibility(View.GONE);
	}

	public void cancelSeekbarTimer() {
		if (seekbarTimer != null) {
			seekbarTimer.cancel();
			seekbarTimer = null;
		}
		if (soundPool != null) {
			soundPool.pause(streamId);
			soundPool.stop(streamId);
			soundid = soundPool.load(mapActivity, R.raw.beep, counter);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	public void onDestroyView() {
		SupportMapFragment f = (SupportMapFragment) getFragmentManager()
				.findFragmentById(R.id.fragmentMap);
		if (f != null) {
			try {
				getFragmentManager().beginTransaction().remove(f).commit();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		mMap = null;
		super.onDestroyView();
	}

	private class newRequestReciever extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			String response = intent.getStringExtra(AndyConstants.NEW_REQUEST);
			AppLog.Log(TAG, "FROM BROAD CAST-->" + response);
			try {
				JSONObject jsonObject = new JSONObject(response);
				if (jsonObject.getInt(AndyConstants.Params.UNIQUE_ID) == 1) {
					requestDetail = parseContent.parseNotification(response);
					if (requestDetail != null) {

						setComponentVisible();

						String destination = requestDetail
								.getDestinationAddress();
						destination = destination.replace("\n", "");
						tvDestinationAddress.setText(getResources().getString(
								R.string.text_to)
								+ " " + destination);

						String source = requestDetail.getSourceAddress();
						source = source.replace("\n", "");
						tvSourceAddress.setText(getResources().getString(
								R.string.text_from)
								+ " " + source);

						btnClientReqRemainTime.setText(""
								+ requestDetail.getTimeLeft());

						tvClientName.setText(requestDetail.getClientName());
						tvTotalPrice.setText(getResources().getString(
								R.string.text_total_price)
								+ " "
								+ getResources().getString(R.string.currency)
								+ requestDetail.getTotalAmount());
						Log.i("CRF", "" + requestDetail.getTotalAmount());
						tvClientNegotiationPrice
								.setText(getResources()
										.getString(
												R.string.text_customer_negotiated_price)
										+ " "
										+ getResources().getString(
												R.string.currency)
										+ requestDetail.getClientAmount());
						Log.i("CRF", "" + requestDetail.getClientAmount());

						if (requestDetail.getClientRating() != 0) {
							tvClientRating.setRating(requestDetail
									.getClientRating());
							Log.i("Rating",
									"" + requestDetail.getClientRating());
						}
						if (TextUtils.isEmpty(requestDetail.getClientProfile())) {
							aQuery.id(ivClientProfilePicture)
									.progress(R.id.pBar).image(R.drawable.user);
						} else {
							aQuery.id(ivClientProfilePicture)
									.progress(R.id.pBar)
									.image(requestDetail.getClientProfile());
						}

						if (markerClientLocation == null) {
							markerClientLocation = mMap
									.addMarker(new MarkerOptions()
											.position(
													new LatLng(
															Double.parseDouble(requestDetail
																	.getClientLatitude()),
															Double.parseDouble(requestDetail
																	.getClientLongitude())))
											.icon(BitmapDescriptorFactory
													.fromResource(R.drawable.pin_client))
											.title(mapActivity
													.getResources()
													.getString(
															R.string.client_location)));
						} else {
							markerClientLocation.setPosition(new LatLng(Double
									.parseDouble(requestDetail
											.getClientLatitude()), Double
									.parseDouble(requestDetail
											.getClientLongitude())));
						}
						if (seekbarTimer == null) {
							seekbarTimer = new SeekbarTimer(
									requestDetail.getTimeLeft() * 1000, 1000);
							seekbarTimer.start();
						}
						AppLog.Log(TAG, "From broad cast recieved request");
					}
				} else {
					setComponentInvisible();
					preferenceHelper.clearRequestData();
					if (markerClientLocation != null
							&& markerClientLocation.isVisible()) {
						markerClientLocation.remove();
						markerClientLocation = null;
					}
					cancelSeekbarTimer();
					removeNotification();
					startCheckingUpcomingRequests();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onLocationReceived(Location location) {
		if (location != null)
			this.location = location;
	}

	@Override
	public void onConntected(Bundle bundle) {

	}

	@Override
	public void onConntected(Location location) {
		this.location = location;
		if (location != null) {
			if (mMap != null) {
				if (markerDriverLocation == null) {
					markerDriverLocation = mMap.addMarker(new MarkerOptions()
							.position(
									new LatLng(location.getLatitude(), location
											.getLongitude()))
							.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.pin_driver))
							.title(getResources().getString(
									R.string.my_location)));
					mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
							new LatLng(location.getLatitude(), location
									.getLongitude()), 12));
				} else {
					markerDriverLocation.setPosition(new LatLng(location
							.getLatitude(), location.getLongitude()));
				}
			}
		} else {
			showLocationOffDialog();
		}
	}

	private void updateButtonUi(boolean state) {
		btnGoOffline.setSelected(state);
		if (btnGoOffline.isSelected()) {
			btnGoOffline.setText(mapActivity.getResources().getString(
					R.string.text_go_offline));
			linearOffline.setVisibility(View.GONE);
			relMap.setVisibility(View.VISIBLE);
		} else {
			btnGoOffline.setText(getString(R.string.text_go_online));
			linearOffline.setVisibility(View.VISIBLE);
			relMap.setVisibility(View.GONE);
		}
	}

	private void checkState() {
		if (!AndyUtils.isNetworkAvailable(mapActivity)) {
			AndyUtils.showToast(
					getResources().getString(R.string.toast_no_internet),
					mapActivity);
			return;
		}
		// AndyUtils.showCustomProgressDialog(mapActivity, "", getResources()
		// .getString(R.string.progress_getting_avaibility), false);
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(AndyConstants.URL,
				AndyConstants.ServiceType.CHECK_STATE + AndyConstants.Params.ID
						+ "=" + preferenceHelper.getUserId() + "&"
						+ AndyConstants.Params.TOKEN + "="
						+ preferenceHelper.getSessionToken());
		new HttpRequester(mapActivity, map,
				AndyConstants.ServiceCode.CHECK_STATE, true, this);
	}

	private void changeState() {
		if (!AndyUtils.isNetworkAvailable(mapActivity)) {
			AndyUtils.showToast(
					getResources().getString(R.string.toast_no_internet),
					mapActivity);
			return;
		}

		AndyUtils.showCustomProgressDialog(mapActivity, "", getResources()
				.getString(R.string.progress_changing_avaibilty), false);

		HashMap<String, String> map = new HashMap<String, String>();
		map.put(AndyConstants.URL, AndyConstants.ServiceType.TOGGLE_STATE);
		map.put(AndyConstants.Params.ID, preferenceHelper.getUserId());
		map.put(AndyConstants.Params.TOKEN, preferenceHelper.getSessionToken());

		new HttpRequester(mapActivity, map,
				AndyConstants.ServiceCode.TOGGLE_STATE, this);
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {

		mMap= googleMap;
		mMap.getUiSettings().setZoomControlsEnabled(false);
		mMap.setMyLocationEnabled(false);
		mMap.getUiSettings().setMyLocationButtonEnabled(false);

		mMap.setInfoWindowAdapter(new InfoWindowAdapter() {
			@Override
			public View getInfoWindow(Marker marker) {
				View v = mapActivity.getLayoutInflater().inflate(
						R.layout.info_window_layout, null);

				((TextView) v).setText(marker.getTitle());
				return v;
			}

			@Override
			public View getInfoContents(Marker marker) {

				// Getting view from the layout file info_window_layout View

				// Getting reference to the TextView to set title TextView

				// Returning the view containing InfoWindow contents return
				return null;

			}

		});

		mMap.setOnMarkerClickListener(new OnMarkerClickListener() {
			@Override
			public boolean onMarkerClick(Marker marker) {
				marker.showInfoWindow();
				return true;
			}
		});
		addMarker();
	}

}
