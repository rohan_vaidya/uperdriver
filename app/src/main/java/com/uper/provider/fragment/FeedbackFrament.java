package com.uper.provider.fragment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.androidquery.AQuery;
import com.uper.provider.R;
import com.uper.provider.base.BaseMapFragment;
import com.uper.provider.model.RequestDetail;
import com.uper.provider.model.TypeInvoice;
import com.uper.provider.parse.AsyncTaskCompleteListener;
import com.uper.provider.parse.HttpRequester;
import com.uper.provider.utills.AndyConstants;
import com.uper.provider.utills.AndyUtils;
import com.uper.provider.utills.AppLog;
import com.uper.provider.widget.MyFontEdittextView;
import com.uper.provider.widget.MyFontTextView;

/**
 * @author Elluminati elluminati.in
 * 
 */
public class FeedbackFrament extends BaseMapFragment implements
		AsyncTaskCompleteListener {

	private MyFontEdittextView etFeedbackComment;
	private ImageView ivDriverImage;
	private RatingBar ratingFeedback;
	private MyFontTextView tvClientName;// tvTime,

	private final String TAG = "FeedbackFrament";
	private AQuery aQuery;
	private MyFontTextView tvAmount;
	private String paymentMode;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View feedbackFragmentView = inflater.inflate(
				R.layout.fragment_feedback, container, false);

		etFeedbackComment = (MyFontEdittextView) feedbackFragmentView
				.findViewById(R.id.etFeedbackComment);
		// tvTime = (MyFontTextView) feedbackFragmentView
		// .findViewById(R.id.tvFeedBackTime);
		// tvDistance = (MyFontTextView) feedbackFragmentView
		// .findViewById(R.id.tvFeedbackDistance);
		tvAmount = (MyFontTextView) feedbackFragmentView
				.findViewById(R.id.tvFeedbackAmount);
		ratingFeedback = (RatingBar) feedbackFragmentView
				.findViewById(R.id.ratingFeedback);
		ivDriverImage = (ImageView) feedbackFragmentView
				.findViewById(R.id.ivFeedbackDriverImage);
		tvClientName = (MyFontTextView) feedbackFragmentView
				.findViewById(R.id.tvClientName);

		mapActivity.setActionBarTitle(getResources().getString(
				R.string.text_feedback));

		feedbackFragmentView.findViewById(R.id.tvFeedbackSubmit)
				.setOnClickListener(this);
		// feedbackFragmentView.findViewById(R.id.tvFeedbackSkip)
		// .setOnClickListener(this);

		return feedbackFragmentView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		aQuery = new AQuery(mapActivity);
		RequestDetail requestDetail = (RequestDetail) getArguments()
				.getSerializable(AndyConstants.REQUEST_DETAIL);

		if (!(TextUtils.isEmpty(requestDetail.getClientProfile()))) {
			aQuery.id(ivDriverImage).image(requestDetail.getClientProfile());
		}

		ArrayList<TypeInvoice> listTypeInvoice = requestDetail
				.getListTypeInvoice();

		TypeInvoice typeInvoice = new TypeInvoice();
		typeInvoice.setName(getString(R.string.text_original_total_price));
		typeInvoice.setBasePrice(Double.parseDouble(requestDetail
				.getOriginalTotal()));
		listTypeInvoice.add(typeInvoice);

		TypeInvoice typeInvoice1 = new TypeInvoice();
		typeInvoice1.setName(getString(R.string.text_your_total_price));
		typeInvoice1.setBasePrice(Double.parseDouble(requestDetail
				.getNegotiationTotal()));
		listTypeInvoice.add(typeInvoice1);

		TypeInvoice typeInvoice2 = new TypeInvoice();
		typeInvoice2.setName(getString(R.string.text_referral_bonus));
		typeInvoice2.setBasePrice(Double.parseDouble(requestDetail
				.getReferralBonus()));
		listTypeInvoice.add(typeInvoice2);

		TypeInvoice typeInvoice3 = new TypeInvoice();
		typeInvoice3.setName(getString(R.string.text_promo_bonus));
		typeInvoice3.setBasePrice(Double.parseDouble(requestDetail
				.getPromoBonus()));
		listTypeInvoice.add(typeInvoice3);

		TypeInvoice typeInvoice4 = new TypeInvoice();
		typeInvoice4.setName(getString(R.string.text_you_saved));
		typeInvoice4.setBasePrice(Double.parseDouble(requestDetail
				.getYouSaved()));
		listTypeInvoice.add(typeInvoice4);

		mapActivity.showBillDialog(requestDetail.getListTypeInvoice(),
				requestDetail.getTotal(), getString(R.string.text_confirm));

		if (preferenceHelper.getPaymentType() == AndyConstants.CASH)
			paymentMode = getString(R.string.text_type_cash);
		else
			paymentMode = getString(R.string.text_type_card);

		// tvAmount.setText("$"
		// + new DecimalFormat("0.00").format(Double
		// .parseDouble(requestDetail.getAmount())) + " "
		// + paymentMode);
		tvClientName.setText(requestDetail.getClientName());
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.tvFeedbackSubmit:

			// if (TextUtils.isEmpty(etFeedbackComment.getText().toString())) {
			// AndyUtils.showToast(
			// mapActivity.getResources().getString(
			// R.string.text_empty_feedback), mapActivity);
			// return;
			if (ratingFeedback.getRating() == 0) {
				AndyUtils.showToast(
						mapActivity.getResources().getString(
								R.string.text_empty_rating), mapActivity);
			} else {
				giveRating();
			}
			break;
		// case R.id.tvFeedbackSkip:
		// preferenceHelper.clearRequestData();
		// mapActivity.addFragment(new ClientRequestFragment(), false,
		// AndyConstants.CLIENT_REQUEST_TAG, true);
		// break;
		default:
			break;
		}
	}

	// giving feedback for perticular job
	private void giveRating() {
		if (!AndyUtils.isNetworkAvailable(mapActivity)) {
			AndyUtils.showToast(
					getResources().getString(R.string.toast_no_internet),
					mapActivity);
			return;
		}

		AndyUtils.showCustomProgressDialog(mapActivity, "", getResources()
				.getString(R.string.progress_rating), false);

		HashMap<String, String> map = new HashMap<String, String>();
		map.put(AndyConstants.URL, AndyConstants.ServiceType.RATING);
		map.put(AndyConstants.Params.ID, preferenceHelper.getUserId());
		map.put(AndyConstants.Params.TOKEN, preferenceHelper.getSessionToken());
		map.put(AndyConstants.Params.REQUEST_ID,
				String.valueOf(preferenceHelper.getRequestId()));
		map.put(AndyConstants.Params.RATING,
				String.valueOf(ratingFeedback.getRating()));
		map.put(AndyConstants.Params.COMMENT, etFeedbackComment.getText()
				.toString().trim());

		new HttpRequester(mapActivity, map, AndyConstants.ServiceCode.RATING,
				this);
	}

	@Override
	public void onTaskCompleted(String response, int serviceCode) {
		AndyUtils.removeCustomProgressDialog();
		switch (serviceCode) {
		case AndyConstants.ServiceCode.RATING:
			AppLog.Log(TAG, "rating response" + response);
			if (parseContent.isSuccess(response)) {
				preferenceHelper.clearRequestData();
				AndyUtils.showToast(
						mapActivity.getResources().getString(
								R.string.toast_feedback_success), mapActivity);
				mapActivity.addFragment(new ClientRequestFragment(), false,
						AndyConstants.CLIENT_REQUEST_TAG, true);
			}

			break;

		default:
			break;
		}
	}
}
