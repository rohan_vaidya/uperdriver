package com.uper.provider.parse;

public interface AsyncTaskCompleteListener {
	void onTaskCompleted(String response, int serviceCode);

}
